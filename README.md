# Codecacao Drive Application
##### Application on startup will prompt to request permission for GPS and FINE LOCATION
* First it is necessary to select the starting and ending locations

![](http://mdrusko1.square7.ch/codecacao/1.png)

* After the booking data is entered, a button for starting the ride will be available.

![](http://mdrusko1.square7.ch/codecacao/2.png)

* After a ride is started, the only option available is "passengers picked up".

![](http://mdrusko1.square7.ch/codecacao/3.png)

* After the passengers are picked up, available options are "stop-over" and "end ride".

![](http://mdrusko1.square7.ch/codecacao/4.png)


#### MENU

* Open menu by clicking on the hamburger button or swiping on the right

![](http://mdrusko1.square7.ch/codecacao/5.png)

#### RIDES

* Previous driving list
* Driving details for each ride
* Deleting selected ride

![](http://mdrusko1.square7.ch/codecacao/6.png)


 #### SETTINGS
* Driver entry
* Driver name
* Driver age
* Driver profile picture
* Vehicle registration plate number

![](http://mdrusko1.square7.ch/codecacao/8.png)
