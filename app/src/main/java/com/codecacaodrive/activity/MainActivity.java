package com.codecacaodrive.activity;

import android.Manifest;
import android.animation.Animator;
import android.app.job.JobScheduler;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteConstraintException;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.codecacaodrive.Application;
import com.codecacaodrive.model.Drive;
import com.codecacaodrive.model.LocationData;
import com.codecacaodrive.R;
import com.codecacaodrive.utils.Functions;
import com.codecacaodrive.utils.Util;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;
import com.squareup.otto.Subscribe;

import java.util.Objects;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Response;
import pl.droidsonroids.gif.GifImageView;

import static com.codecacaodrive.Application.bus;
import static com.codecacaodrive.Application.db;

public class MainActivity extends AppCompatActivity {
    private String bookingId, lat, lng, startTime;
    private String status = "idle";
    private ResideMenu resideMenu;

    @BindView(R.id.lblLongitudeValueGPS)
    TextView lblLongitudeValueGPS;
    @BindView(R.id.lblLatitudeValueGPS)
    TextView lblLatitudeValueGPS;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.lblStartLocation)
    TextView lblstartLocation;
    @BindView(R.id.lblendLocation)
    TextView lblEndLocation;
    @BindView(R.id.txtPassengers)
    TextView txtPassengers;
    @BindView(R.id.fabStart)
    FloatingActionButton fabStart;
    @BindView(R.id.fabPickUp)
    FloatingActionButton fabPickUp;
    @BindView(R.id.fabStop)
    FloatingActionButton fabStop;
    @BindView(R.id.gifCar)
    GifImageView gifCar;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setupToolbar();

        // Register Event Bus
        Application.bus.register(this);

        // Decorate Status Bar
        createTranparentStatusBar();

        // Create menu items
        createResideMenu();

        // Check permissions
        checkLocationPermission();
    }


    private void sendDataToServer() {
        AndroidNetworking.get(Application.BASE_URL)
                .addQueryParameter("bookingId", bookingId)
                .addQueryParameter("status", status)
                .addQueryParameter("time", Functions.getCurrentDateTime())
                .addQueryParameter("lat", lat)
                .addQueryParameter("lng", lng)
                .setPriority(Priority.MEDIUM)
                .build().getAsOkHttpResponse(new OkHttpResponseListener() {

            @Override
            public void onResponse(Response response) {

                if (response.code() != 200) {
                    Log.d("RESPONSE",  "Server error!");
                }

                Log.d("RESPONSE", response.code() + "");
            }

            @Override
            public void onError(ANError anError) {
                Log.d("RESPONSE", anError.getErrorBody());
            }
        });

    }


    @OnClick(R.id.lblStartLocation)
    public void setLblstartLocationOnClick() {
        getLocation(0);
    }

    @OnClick(R.id.lblendLocation)
    public void setlblendLocationOnClick() {
        getLocation(1);
    }

    @OnClick(R.id.fabStart)
    public void fabStartOnClick() {

        if (status.equals("pause")) {
            status = "continue";
            sendDataToServer();
            changeCarStatus();
            return;
        }

        if (status.equals("running") || status.equals("continue") || status.equals("pickedUp")) {
            status = "pause";
            changeCarStatus();
            sendDataToServer();
            return;
        }


        if (status.equals("ready")) {

            // Disable From / End Locations
            lblstartLocation.setEnabled(false);
            lblEndLocation.setEnabled(false);

            // Start driving
            startTime = Functions.getCurrentDateTime();
            status = "running";

            if (bookingId == null) {
                //Create random bookingId
                Random rand = new Random();
                int n = rand.nextInt(20); // Gives n such that 0 <= n < 20
                bookingId = String.valueOf(n);
            }
            changeCarStatus();
            sendDataToServer();
        }
    }


    @OnClick(R.id.fabPickUp)
    public void fabPickUpOnClick() {
        int passengers = Integer.parseInt(txtPassengers.getText().toString());
        passengers++;
        txtPassengers.setText(String.valueOf(passengers));

        if (!status.equals("pickedUp")) {
            status = "pickedUp";
            changeCarStatus();
        }

        sendDataToServer();
    }


    @OnClick(R.id.fabStop)
    public void fabStopClick() {
        status = "stop";
        sendDataToServer();
        changeCarStatus();
    }

    private void getLocation(int code) {

        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS).build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(typeFilter)
                    .build(this);
            startActivityForResult(intent, code);
        } catch (GooglePlayServicesRepairableException e) {
            Log.d("Error", "Error accure: " + e.getMessage());
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.d("Error", "GooglePlayServicesNotAvailableException " + e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            Place place = PlaceAutocomplete.getPlace(this, data);

            // Set address to start location
            if (requestCode == 0) {
                lblstartLocation.setText(place.getAddress());
            }

            // Set address to end location
            if (requestCode == 1) {
                lblEndLocation.setText(place.getAddress());
            }


        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(this, data);
            Toast.makeText(this, status.getStatusMessage(), Toast.LENGTH_SHORT).show();

        } else if (resultCode == RESULT_CANCELED) {
            // The user canceled the operation.
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setLogo(R.drawable.ic_hamburger);
        toolbar.setOnClickListener(view -> resideMenu.openMenu(0));
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
        }

        @Override
        public void closeMenu() {
        }
    };

    private void createResideMenu() {
        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.wallpaper);
        resideMenu.attachToActivity(this);
        resideMenu.setMenuListener(menuListener);

        final String titles[] = {"Rides", "Settings"};
        int icon[] = {R.drawable.ic_car, R.drawable.ic_settings};

        for (int i = 0; i < titles.length; i++) {
            ResideMenuItem item = new ResideMenuItem(this, icon[i], titles[i]);
            final int finalI = i;
            item.setOnClickListener(view -> {
                if (titles[finalI].equals("Rides")) {
                    YoYo.with(Techniques.TakingOff)
                            .duration(700)
                            .withListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    startActivity(new Intent(MainActivity.this, RidesActivity.class));
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {
                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {
                                }
                            })
                            .playOn(resideMenu.getMenuItems().get(0));

                }


                if (titles[finalI].equals("Settings")) {
                    YoYo.with(Techniques.TakingOff)
                            .duration(700)
                            .withListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    startActivity(new Intent(MainActivity.this, DriverActivity.class));
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {
                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            })
                            .playOn(resideMenu.getMenuItems().get(1));

                }

            });

            resideMenu.addMenuItem(item, ResideMenu.DIRECTION_LEFT);
        }
    }


    // Permissions
    private void checkLocationPermission() {
        if (!permissionsGranted()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        } else {
            checkGPS();
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // Location permission granted.
                Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show();

                // Check if GPS is enabled
                checkGPS();

            } else {

                // User refused to grant permission.
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setTitle("Location permission error!");
                alertDialogBuilder
                        .setMessage("In order to use this app location permission must be enabled")
                        .setCancelable(false)
                        .setPositiveButton("OK", (dialog1, id1) -> {
                            dialog1.dismiss();
                            checkLocationPermission();
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        }
    }

    private void checkGPS() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            checkForInputData();
        }
    }

    // Permission dialog
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNeutralButton("Exit", (dialog, which) -> System.exit(0))
                .setNegativeButton("No", (dialog, id) -> {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    alertDialogBuilder.setTitle("GPS error!");
                    alertDialogBuilder
                            .setMessage("In order to use this app GPS must be enabled")
                            .setCancelable(false)
                            .setPositiveButton("OK", (dialog1, id1) -> {
                                dialog1.dismiss();
                                checkGPS();
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private Boolean permissionsGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    protected void onResume() {
        super.onResume();

        YoYo.with(Techniques.FadeIn)
                .duration(1)
                .playOn(resideMenu.getMenuItems().get(0));

        YoYo.with(Techniques.FadeIn)
                .duration(1)
                .playOn(resideMenu.getMenuItems().get(1));
    }


    @Subscribe
    public void getMessage(LocationData locationData) {

        // Get location data from Location reciever
        if (locationData != null) {
            lat = locationData.getLatitude();
            lng = locationData.getLongitude();

            lblLatitudeValueGPS.setText("Latitude: " + lat);
            lblLongitudeValueGPS.setText("Longitude: " + lng);

            sendDataToServer();
        }
    }


    private void changeCarStatus() {

        switch (status) {

            case "idle":
                resetControlsToStartPossition();
                break;

            case "ready":
                break;

            case "running":
                // Start sending data to server
                Util.scheduleJob(MainActivity.this);

                //Change icon to pause
                fabStart.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_pause));
                fabStart.setVisibility(View.INVISIBLE);

                //Show passenger button
                fabPickUp.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.Landing)
                        .repeat(0)
                        .duration(1000)
                        .playOn(fabPickUp);

                //Show running car image
                gifCar.setVisibility(View.VISIBLE);

                if (lat == null && lng == null) {
                    lblLatitudeValueGPS.setText("Loading data...");
                    lblLongitudeValueGPS.setText("Loading data...");
                }
                break;

            case "pickedUp":
                // Show pause and stop buttons
                fabStart.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.Landing)
                        .repeat(0)
                        .duration(1000)
                        .playOn(fabStart);

                fabStop.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.Landing)
                        .repeat(0)
                        .duration(1000)
                        .playOn(fabStop);
                break;


            case "pause":
                //Change icon to play
                fabStart.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play));
                fabPickUp.setVisibility(View.INVISIBLE);
                fabStop.setVisibility(View.INVISIBLE);
                gifCar.setVisibility(View.INVISIBLE);
                break;

            case "continue":
                fabStart.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_pause));
                fabPickUp.setVisibility(View.VISIBLE);
                fabStop.setVisibility(View.VISIBLE);
                gifCar.setVisibility(View.VISIBLE);
                break;

            case "stop":
                Drive drive = new Drive();
                drive.setStartLocation(lblstartLocation.getText().toString());
                drive.setEndLocation(lblEndLocation.getText().toString());
                drive.setPassengers(txtPassengers.getText().toString());
                drive.setLat(lat);
                drive.setLng(lng);
                drive.setStartTime(startTime);
                drive.setEndTime(Functions.getCurrentDateTime());

                resetControlsToStartPossition();

                stopService();

                saveRide(drive);

                checkForInputData();

                break;

        }
    }


    private void checkForInputData() {

        Timer t = new Timer();
        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(() -> {

                    // Check if data was entered
                    if (!TextUtils.isEmpty(lblstartLocation.getText().toString()) && (!TextUtils.isEmpty(lblEndLocation.getText().toString()))) {
                        fabStart.setVisibility(View.VISIBLE);
                        YoYo.with(Techniques.Bounce)
                                .repeat(1)
                                .duration(1000)
                                .playOn(fabStart);
                        status = "ready";
                        t.cancel();
                    }

                });

            }

        };

        t.scheduleAtFixedRate(task, 0, 2000);

    }


    private void saveRide(Drive currentDrive) {

        AsyncTask.execute(() -> {
            try {
                db.databaseInterface().insertAll(currentDrive);
                runOnUiThread(() -> {
                    // here I am displaying it
                    Toast.makeText(MainActivity.this, "Drive complete. Data saved!", Toast.LENGTH_SHORT).show();
                });

            } catch (SQLiteConstraintException ex) {
                runOnUiThread(() -> Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show());
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
        stopService();
    }


    private void stopService() {
        JobScheduler jobScheduler = (JobScheduler) MainActivity.this.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobScheduler != null) {
            jobScheduler.cancelAll();
        }
    }

    private void resetControlsToStartPossition() {

        fabStart.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play));

        gifCar.setVisibility(View.INVISIBLE);
        fabPickUp.setVisibility(View.INVISIBLE);
        fabStart.setVisibility(View.INVISIBLE);
        fabStop.setVisibility(View.INVISIBLE);

        lblstartLocation.setEnabled(true);
        lblEndLocation.setEnabled(true);

        lblstartLocation.setText("");
        lblEndLocation.setText("");
        txtPassengers.setText("0");
        lblLatitudeValueGPS.setText("00.000000");
        lblLongitudeValueGPS.setText("00.000000");

        fabStop.setTag("");
        fabPickUp.setTag("");
        fabStart.setTag("");

        status = "stop";

    }


    // Decoration
    private void createTranparentStatusBar() {
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        ViewGroup contentParent = findViewById(android.R.id.content);
        View content = contentParent.getChildAt(0);
        setFitsSystemWindows(content, false, true);
        clipToStatusBar(toolbar);
    }

    protected void setFitsSystemWindows(View view, boolean fitSystemWindows,
                                        boolean applyToChildren) {
        if (view == null) return;
        view.setFitsSystemWindows(fitSystemWindows);
        if (applyToChildren && (view instanceof ViewGroup)) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0, n = viewGroup.getChildCount(); i < n; i++) {
                viewGroup.getChildAt(i).setFitsSystemWindows(fitSystemWindows);
            }
        }
    }

    protected void clipToStatusBar(View view) {
        final int statusBarHeight = getStatusBarHeight(this);
        view.getLayoutParams().height += statusBarHeight;
        view.setPadding(0, statusBarHeight, 0, 0);
    }

    protected int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
