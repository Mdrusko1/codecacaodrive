package com.codecacaodrive.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.codecacaodrive.model.Drive;
import com.codecacaodrive.R;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RideDetailsActivity extends AppCompatActivity {

    private Drive drive;

    @BindView(R.id.imgCar)
    ImageView imageViewCar;

    @BindView(R.id.txtStartLocationDetails)
    TextView txtStartLocationDetails;

    @BindView(R.id.txtEndLocationDetails)
    TextView txtEndLocationDetails;

    @BindView(R.id.txtLatitudeDetails)
    TextView txtLatitudeDetails;

    @BindView(R.id.txtLongitudeDetails)
    TextView txtLongitudeDetails;

    @BindView(R.id.txtStartTimeDetails)
    TextView txtStartTimeDetails;

    @BindView(R.id.txtEndTimeDetails)
    TextView txtEndTimeDetails;

    @BindView(R.id.txtPassengersDetails)
    TextView txtPassengersDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_details);
        ButterKnife.bind(this);
        setupToolbar();
        supportStartPostponedEnterTransition();

        getDetailsData();

    }

    private void setupToolbar() {
        // Add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void getDetailsData() {
        Gson gson = new Gson();
        String strObj = getIntent().getStringExtra("Drive");
        drive = gson.fromJson(strObj, Drive.class);

        txtStartLocationDetails.setText(getString(R.string.from) + " " + drive.getStartLocation());
        txtEndLocationDetails.setText(getString(R.string.to) + " " + drive.getEndLocation());
        txtLatitudeDetails.setText(getString(R.string.latitude) + " " + drive.getLat());
        txtLongitudeDetails.setText(getString(R.string.longitude) + " " + drive.getLng());
        txtStartTimeDetails.setText(getString(R.string.start_time) + " " + drive.getStartTime());
        txtEndTimeDetails.setText(getString(R.string.end_time) + " " + drive.getEndTime());
        txtPassengersDetails.setText(getString(R.string.passengers) + " " + drive.getPassengers());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
