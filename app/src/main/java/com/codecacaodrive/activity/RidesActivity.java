package com.codecacaodrive.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import com.codecacaodrive.adapter.RidesRecyclerViewAdapter;
import com.codecacaodrive.model.Drive;
import com.codecacaodrive.R;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.codecacaodrive.Application.bus;
import static com.codecacaodrive.Application.db;

public class RidesActivity extends AppCompatActivity {

    private List<Drive> driveList = new ArrayList<>();
    private RidesRecyclerViewAdapter adapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rides);
        ButterKnife.bind(this);
        bus.register(this);
        setupToolbar();

        swipeRefreshLayout.setOnRefreshListener(this::getDrives);
        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorDarkBlue

        );

        getDrives();

    }

    private void setupToolbar() {
        // Add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }


    private void getDrives() {
        AsyncTask.execute(() -> {
            List<Drive> drives = db.databaseInterface().getDrives();
            runOnUiThread(() -> {

                driveList.clear();

                try {
                    for (int i = 0; i < drives.size(); i++) {
                        Drive drive = new Drive();
                        drive.setId(drives.get(i).getId());
                        drive.setStartLocation(drives.get(i).getStartLocation());
                        drive.setEndLocation(drives.get(i).getEndLocation());
                        drive.setStartTime(drives.get(i).getStartTime());
                        drive.setEndTime(drives.get(i).getEndTime());
                        drive.setPassengers(drives.get(i).getPassengers());
                        drive.setLat(drives.get(i).getLat());
                        drive.setLng(drives.get(i).getLng());

                        driveList.add(drive);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(RidesActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                setAdapter();
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            });
        });
    }


    @Subscribe
    public void getMessage(String command) {

        if (command.equals("refresh_drives_data")) {
            getDrives();
        }

    }

    private void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RidesRecyclerViewAdapter(this, driveList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}


