package com.codecacaodrive.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.codecacaodrive.Application;
import com.codecacaodrive.R;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriverActivity extends AppCompatActivity {
    @BindView(R.id.imgDriver)
    ImageView imgDriver;
    @BindView(R.id.txtDriverName)
    EditText txtDriverName;
    @BindView(R.id.txtDriverAge)
    EditText txtDriverAge;
    @BindView(R.id.txtDriverPlate)
    EditText txtDriverPlate;
    @BindView(R.id.btnOK)
    Button btnOK;

    private Uri imageUri;
    private String driverImgPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);
        setupToolbar();
        ButterKnife.bind(this);

        isStoragePermissionGranted();
        getDriverInformationData();

    }

    private void setupToolbar() {
        // Add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }


    private void getDriverInformationData() {

        //Load driver data from shared preferences
        SharedPreferences prefs = getSharedPreferences(Application.pref_image, MODE_PRIVATE);
        String imgPath = prefs.getString("driver_profile_img_path", "");
        String driverName = prefs.getString("driver_profile_name", "");
        String driverAge = prefs.getString("driver_profile_age", "");
        String driverPlate = prefs.getString("driver_profile_plate", "");
        Bitmap bitmap = BitmapFactory.decodeFile(imgPath);

        txtDriverName.setText(driverName);
        txtDriverAge.setText(driverAge);
        txtDriverPlate.setText(driverPlate);

        if (bitmap != null) {
            imgDriver.setImageBitmap(bitmap);
        }
    }

    @OnClick(R.id.btnOK)
    public void btnOKClick() {

        if (txtDriverName.getText().toString().length() == 0) {
            txtDriverName.setError("Enter driver name");
            txtDriverName.requestFocus();
            return;
        }

        if (txtDriverAge.getText().toString().length() == 0) {
            txtDriverAge.setError("Enter driver age");
            txtDriverAge.requestFocus();
            return;
        }

        if (txtDriverPlate.getText().toString().length() == 0) {
            txtDriverPlate.setError("Enter driver registration plate");
            txtDriverPlate.requestFocus();
            return;
        }

        SharedPreferences.Editor editor = getSharedPreferences(Application.pref_image, MODE_PRIVATE).edit();
        editor.putString("driver_profile_name", txtDriverName.getText().toString());
        editor.putString("driver_profile_age", txtDriverAge.getText().toString());
        editor.putString("driver_profile_plate", txtDriverPlate.getText().toString());

        if (imageUri != null) {
            editor.putString("driver_profile_img_path", driverImgPath);
        }

        Toast.makeText(this, "Driver " + txtDriverName.getText().toString() + " saved!", Toast.LENGTH_SHORT).show();

        editor.apply();
        finish();
    }


    @OnClick(R.id.imgDriver)
    public void imgDriverClick() {
        startActivityForResult(new Intent(Intent.ACTION_PICK).setType("image/*"), 0);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && data != null) {
            imageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                imgDriver.setImageBitmap(bitmap);
                driverImgPath = getRealPathFromURI(this, data.getData());
            } catch (IOException e) {
                Toast.makeText(this, "Error getting image!", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

    }


    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation

            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //resume tasks needing this permission
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
