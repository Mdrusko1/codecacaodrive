package com.codecacaodrive;

import android.arch.persistence.room.Room;

import com.androidnetworking.AndroidNetworking;
import com.codecacaodrive.database.AppDatabase;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import okhttp3.OkHttpClient;

public class Application extends android.app.Application {

    public static String BASE_URL = "https://test.mother.i-ways.hr?json=1";
    public static String pref_image;
    public static Bus bus = new Bus(ThreadEnforcer.ANY);
    public static int service_interval = 7000;
    public static AppDatabase db;
    @Override
    public void onCreate() {
        super.onCreate();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().build();
        AndroidNetworking.initialize(getApplicationContext(), okHttpClient);

        // Database
        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "database").build();
    }
}
