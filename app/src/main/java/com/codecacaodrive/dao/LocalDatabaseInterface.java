package com.codecacaodrive.dao;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.codecacaodrive.model.Drive;

import java.util.List;

@Dao
public interface LocalDatabaseInterface {

    @Query("SELECT * FROM drive")
    List<Drive> getDrives();

    @Insert
    void insertAll(Drive... drives);

    @Delete
    void deleteItem(Drive... drives);

}
