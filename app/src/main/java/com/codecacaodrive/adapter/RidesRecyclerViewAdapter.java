package com.codecacaodrive.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codecacaodrive.activity.RideDetailsActivity;
import com.codecacaodrive.model.Drive;
import com.codecacaodrive.R;
import com.google.gson.Gson;

import java.util.List;

import static com.codecacaodrive.Application.bus;
import static com.codecacaodrive.Application.db;

public class RidesRecyclerViewAdapter extends RecyclerView.Adapter<RidesRecyclerViewAdapter.ViewHolder> {

    private List<Drive> mData;
    private LayoutInflater mInflater;
    private Context mContext;

    public RidesRecyclerViewAdapter(Context context, List<Drive> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.lblStartPosition.setText(mData.get(position).getStartLocation());
        holder.lblEndPosition.setText(mData.get(position).getEndLocation());

        holder.itemView.setOnClickListener(view -> {
            // Pass data object in the bundle and populate details activity.
            Intent intent = new Intent(mContext, RideDetailsActivity.class);
            Gson gson = new Gson();
            intent.putExtra("Drive", gson.toJson(mData.get(position)));
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) mContext, holder.imageView, "simple_activity_transition");
            mContext.startActivity(intent, options.toBundle());

        });


        //Delete ride
        holder.imageViewDelete.setOnClickListener(v -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
            builder.setTitle("Delete route:\n" + mData.get(position).getStartLocation() + " - " + mData.get(position).getEndLocation())
                    .setMessage("Are you sure you want to delete this ride?")
                    .setPositiveButton("Yes", (dialog, which) -> new Thread(() -> {
                        Drive drive = mData.get(position);
                        db.databaseInterface().deleteItem(drive);
                        bus.post("refresh_drives_data");

                    }).start())

                    .setNegativeButton("No", (dialog, which) -> {
                        // do nothing
                        dialog.dismiss();
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        });
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView lblStartPosition;
        private TextView lblEndPosition;
        private ImageView imageView;
        private ImageView imageViewDelete;


        private ViewHolder(View itemView) {
            super(itemView);
            lblStartPosition = itemView.findViewById(R.id.txtStartLocation);
            lblEndPosition = itemView.findViewById(R.id.txtEndLocation);
            imageView = itemView.findViewById(R.id.imageView);
            imageViewDelete = itemView.findViewById(R.id.imageViewDelete);
        }
    }

    public Drive getItem(int id) {
        return mData.get(id);
    }

}

