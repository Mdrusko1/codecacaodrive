package com.codecacaodrive.services;

import android.Manifest;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.codecacaodrive.model.LocationData;
import com.codecacaodrive.utils.Util;

import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;

import static com.codecacaodrive.Application.bus;


public class CarService extends JobService {
    LocationTracker tracker;

    @Override
    public boolean onStartJob(JobParameters params) {
        startTracking();
        Util.scheduleJob(getApplicationContext()); // reschedule the job
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {

        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    private void startTracking() {

        if (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            TrackerSettings settings = new TrackerSettings()
                    .setUseGPS(true)
                    .setUseNetwork(true)
                    .setUsePassive(true)
                    .setTimeBetweenUpdates(2000)
                    .setMetersBetweenUpdates(100);


            tracker = new LocationTracker(getApplicationContext(), settings) {
                @Override
                public void onLocationFound(@NonNull Location location) {
                    LocationData locationData = new LocationData();
                    locationData.setLatitude(String.valueOf(location.getLatitude()));
                    locationData.setLongitude(String.valueOf(location.getLongitude()));

                    bus.post(locationData);
                }

                @Override
                public void onTimeout() {

                }
            };

            tracker.startListening();
        }
    }
}
